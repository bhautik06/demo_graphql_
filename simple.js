const { graphql, buildSchema } = require("graphql");
const { query } = require("express");

const schema = buildSchema(`
  type Query {
    hello: String
  }
`);

const root = {
    hello: () => {
        return "Hello World";
    }
};

graphql(schema, `{hello}`, root).then((result) => {
    console.log(result);
});
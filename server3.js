const express = require("express");
const app = express();
const { graphqlHTTP } = require("express-graphql");
const { buildSchema } = require("graphql");

const schema = buildSchema(`
    input MessageInput {
        content: String
        author: String
    }

    type Message {
        id: ID!
        content: String
        author: String
    }

    type Query {
        getMessage(id: ID!): Message
    }

    type Mutation {
        createMessage(input: MessageInput): Message
        updateMessage(id: ID!, input: MessageInput): Message
    }
`);

class Message {
  constructor(id, { content, author }) {
    this.id = id;
    this.content = content;
    this.author = author;
  }
}

const db = {};

const root = {
  getMessage: ({ id }) => {
    if (!db[id]) {
      throw new Error("no message exists with id " + id);
    }
    return new Message(id, db[id]);
  },
  createMessage: ({ input }) => {
    const id = require("crypto").randomBytes(10).toString("hex");

    db[id] = input;
    return new Message(id, input);
  },
  updateMessage: ({id, input}) => {
      if (!db[id]) {
          throw new Error('No Message exist with id: ' + id)
      }
      return new Message(id, input);
  }
};

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  })
);

app.listen(3000, () => {
  console.log("Server running on port 3000");
});
